package by.my.elections.data.datasource.firebase

import by.my.elections.data.datasource.firebase.model.User
import by.my.elections.data.datasource.schedule.SchedulerProvider
import by.my.elections.data.model.DeviceLocation
import by.my.elections.data.model.UserRole
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.toObject
import durdinapps.rxfirebase2.RxFirestore
import io.reactivex.Completable
import io.reactivex.Observable
import timber.log.Timber
import java.util.*

class FirestoreDataSource(
    private val firebaseFirestore: FirebaseFirestore,
    private val schedulerProvider: SchedulerProvider
) {

    fun createOrUpdateUser(user: User): Completable {
        return RxFirestore.setDocument(
            firebaseFirestore.collection(COLLECTIONS_PATH_TO_USERS).document(user.uuid),
            user.apply { this.lastSignIn = Date() }
        )
            .subscribeOn(schedulerProvider.io())
    }

    fun updateLocation(userId: String, deviceLocation: DeviceLocation): Completable {
        Timber.d("L: updateLocation")
        return RxFirestore.setDocument(
            firebaseFirestore.collection(COLLECTIONS_PATH_TO_LOCATIONS).document(userId),
            deviceLocation.toLocation()
        ).subscribeOn(schedulerProvider.io())
    }

    fun deleteLocation(userId: String): Completable {
        Timber.d("L: deleteLocation")
        return RxFirestore.deleteDocument(
            firebaseFirestore.collection(COLLECTIONS_PATH_TO_LOCATIONS).document(userId)
        ).subscribeOn(schedulerProvider.io())
    }

    fun role(userId: String): Observable<UserRole> {
        return RxFirestore.observeDocumentRef(
            firebaseFirestore.collection(COLLECTIONS_PATH_TO_ROLES).document(userId)
        )
            .map {
                it.toObject<UserRole>() ?: UserRole()
            }
            .toObservable()
            .distinctUntilChanged()
    }

    companion object {
        private const val COLLECTIONS_PATH_TO_USERS = "users"
        private const val COLLECTIONS_PATH_TO_LOCATIONS = "locations"
        private const val COLLECTIONS_PATH_TO_ROLES = "roles"
    }

    private data class Location(
        @PropertyName("location")
        val location: GeoPoint = GeoPoint(0.0, 0.0),
        @PropertyName("lastUpdatedTime")
        val lastUpdatedTime: Date = Date()
    )

    private fun DeviceLocation.toLocation(): Location {
        return Location(location = GeoPoint(latitude, longitude), lastUpdatedTime = Date())
    }


}