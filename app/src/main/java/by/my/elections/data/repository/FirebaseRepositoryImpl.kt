package by.my.elections.data.repository

import by.my.elections.data.datasource.firebase.AuthDataSource
import by.my.elections.data.datasource.firebase.FirestoreDataSource
import by.my.elections.data.datasource.firebase.model.*
import by.my.elections.data.datasource.storage.SecureStorageDataSource
import by.my.elections.data.datasource.storage.exception.NotCachedValueException
import by.my.elections.data.model.DeviceLocation
import by.my.elections.data.model.UserRole
import by.my.elections.domain.repository.AuthRepository
import by.my.elections.domain.repository.FirebaseRepository
import io.reactivex.*

class FirebaseRepositoryImpl(
    private val firestoreDataSource: FirestoreDataSource,
    private val authDataSource: AuthDataSource
) : FirebaseRepository {
    override fun onUserRoleChanged(): Observable<UserRole> {
        return authDataSource.user()
            .flatMapObservable {
                firestoreDataSource.role(it.uuid)
            }
    }

    override fun updateLocation(deviceLocation: DeviceLocation): Completable {
        return authDataSource.user()
            .flatMapCompletable {
                firestoreDataSource.updateLocation(it.uuid, deviceLocation)
            }
    }

    override fun deleteLocation(): Completable {
        return authDataSource.user()
            .flatMapCompletable {
                firestoreDataSource.deleteLocation(it.uuid)
            }
    }
}