package by.my.elections.domain.usecases

import by.my.elections.domain.repository.FirebaseRepository
import by.my.elections.domain.repository.LocationRepository
import io.reactivex.Completable

class DeactivateTrackingUseCase(
    private val firebaseRepository: FirebaseRepository
) :
    BaseCompletableUseCase {
    override fun execute(): Completable {
        return firebaseRepository.deleteLocation()
    }

}