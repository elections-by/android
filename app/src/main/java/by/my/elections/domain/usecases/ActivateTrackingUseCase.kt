package by.my.elections.domain.usecases

import by.my.elections.domain.repository.FirebaseRepository
import by.my.elections.domain.repository.LocationRepository
import io.reactivex.Completable

class ActivateTrackingUseCase(
    private val locationRepository: LocationRepository,
    private val firebaseRepository: FirebaseRepository
) :
    BaseCompletableUseCase {
    override fun execute(): Completable {
        return locationRepository.location()
            .distinctUntilChanged()
            .switchMapCompletable {
                firebaseRepository.updateLocation(it)
            }
    }

}