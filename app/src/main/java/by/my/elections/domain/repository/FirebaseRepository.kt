package by.my.elections.domain.repository

import by.my.elections.data.model.DeviceLocation
import by.my.elections.data.model.UserRole
import io.reactivex.Completable
import io.reactivex.Observable

interface FirebaseRepository {
    fun onUserRoleChanged(): Observable<UserRole>
    fun updateLocation(deviceLocation: DeviceLocation): Completable
    fun deleteLocation(): Completable
}