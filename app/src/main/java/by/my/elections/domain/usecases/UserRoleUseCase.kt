package by.my.elections.domain.usecases

import by.my.elections.domain.model.UserRole
import by.my.elections.domain.repository.FirebaseRepository
import io.reactivex.Observable
import java.util.*

class UserRoleUseCase(private val firebaseRepository: FirebaseRepository) :
    BaseObservableUseCase<UserRole> {
    override fun execute(): Observable<UserRole> {
        return firebaseRepository.onUserRoleChanged()
            .map {
                when (it.type.toLowerCase(Locale.getDefault())) {
                    Guest -> UserRole.Guest
                    Member -> UserRole.Member
                    else -> UserRole.Invalid
                }
            }
            .distinctUntilChanged()
    }

    companion object {
        private const val Guest = "guest"
        private const val Member = "member"
    }
}