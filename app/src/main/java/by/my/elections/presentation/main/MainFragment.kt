package by.my.elections.presentation.main

import android.os.Bundle
import android.view.*
import android.widget.CompoundButton
import androidx.databinding.DataBindingUtil
import by.my.elections.R
import by.my.elections.data.model.DeviceLocation
import by.my.elections.databinding.FragmentMainBinding
import by.my.elections.domain.model.TrackModeStatus
import by.my.elections.presentation.base.BaseFragment
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.*
import com.google.android.material.switchmaterial.SwitchMaterial
import com.jakewharton.rxbinding3.view.clicks
import com.tbruyelle.rxpermissions2.Permission
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.koin.android.ext.android.inject
import timber.log.Timber


class MainFragment : BaseFragment<MainPresenter.View, MainPresenter>(), MainPresenter.View,
    OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
    CompoundButton.OnCheckedChangeListener {
    override val presenter: MainPresenter by inject()
    override val abstractView: MainPresenter.View
        get() = this

    lateinit var binding: FragmentMainBinding
    lateinit var supportSheet: BottomSheetBehavior<View>
    lateinit var activateSheet: BottomSheetBehavior<View>

    private val rxPermission by lazy {
        RxPermissions(this)
    }

    private lateinit var googleMap: GoogleMap

    private val mapReadySubject = PublishSubject.create<Unit>()

    private val switchSubject = BehaviorSubject.create<Boolean>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_main,
            container,
            false
        )
        supportSheet = from(binding.mainSheetSupport.root)
        activateSheet = from(binding.mainSheetActivate.root)
        binding.mainSheetActivate.activateSwitch.setOnCheckedChangeListener(this)
        switchSubject.onNext(false)

        val mapFragment: SupportMapFragment =
            childFragmentManager.findFragmentById(R.id.main_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return binding.root
    }

    override fun onMapReady(map: GoogleMap) {
        map.setMinZoomPreference(6.0f)
        map.setMaxZoomPreference(21.0f)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(53.391667, 27.62902), 8f))
        map.setOnMyLocationButtonClickListener(this)
        map.setOnMapLoadedCallback { mapReadySubject.onNext(Unit) }
        googleMap = map
    }

    override fun onMyLocationButtonClick(): Boolean {
        return false
    }

    override fun onMapReady(): Observable<Unit> {
        return mapReadySubject.hide()
    }

    override fun requestPermission(): Observable<Permission> {
        return rxPermission.requestEach(android.Manifest.permission.ACCESS_FINE_LOCATION)
    }

    override fun hasPermission(): Single<Boolean> {
        return Single.just(rxPermission.isGranted(android.Manifest.permission.ACCESS_FINE_LOCATION))
    }

    override fun showDeviceLocation(deviceLocation: DeviceLocation) {
        googleMap.isMyLocationEnabled = true
        googleMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    deviceLocation.latitude,
                    deviceLocation.longitude
                ), 18f
            )
        )
    }

    override fun showActiveButton() {
        binding.apply {
            mainSheetSupport.root.visibility = View.GONE
            mainSheetActivate.root.visibility = View.VISIBLE
        }
    }

    override fun switchActiveBottomSheet() {
        activateSheet.switch()
    }

    override fun showSupportButton() {
        binding.apply {
            mainSheetSupport.root.visibility = View.VISIBLE
            mainSheetActivate.root.visibility = View.GONE
        }
    }

    override fun switchSupportBottomSheet() {
        supportSheet.switch()
    }

    override fun onSupportClick(): Observable<Unit> {
        return binding.mainSheetSupport.supportButton.clicks()
    }

    override fun onActivateClick(): Observable<Unit> {
        return binding.mainSheetActivate.activateButton.clicks()
    }

    override fun setTrackMode(mode: TrackModeStatus) {
        binding.mainSheetActivate.apply {
            when (mode) {
                is TrackModeStatus.Off -> {
                    activateSwitchDescription.setText(R.string.enable_location_switch_title)
                    activateSwitch.isEnabled = true
                    activateSwitch.off()
                }
                is TrackModeStatus.On -> {
                    activateSwitchDescription.setText(R.string.disable_location_switch_title)
                    activateSwitch.isEnabled = true
                    activateSwitch.on()
                }
                is TrackModeStatus.NoLocation -> {
                    activateSwitchDescription.setText(R.string.location_is_required_switch_title)
                    activateSwitch.isEnabled = false
                    activateSwitch.off()
                }
                is TrackModeStatus.PermissionNotGranted -> {
                    activateSwitchDescription.setText(R.string.no_permission_location_switch_title)
                    activateSwitch.isEnabled = false
                    activateSwitch.off()
                }
            }
        }
    }

    override fun toggle(): Observable<Boolean> {
        return switchSubject.distinctUntilChanged()
    }

    private fun BottomSheetBehavior<View>.switch() {
        when (state) {
            STATE_HIDDEN -> state = STATE_COLLAPSED
            STATE_COLLAPSED -> state = STATE_EXPANDED
            STATE_EXPANDED -> state = STATE_COLLAPSED
            else -> {
                Timber.d("Do nothing: $state")
            }
        }
    }

    private fun SwitchMaterial.off() {
        isChecked = false
    }

    private fun SwitchMaterial.on() {
        isChecked = true
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        switchSubject.onNext(isChecked)
    }
}


