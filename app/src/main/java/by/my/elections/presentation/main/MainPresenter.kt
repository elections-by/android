package by.my.elections.presentation.main

import by.my.elections.data.datasource.schedule.SchedulerProvider
import by.my.elections.data.model.DeviceLocation
import by.my.elections.domain.model.TrackModeStatus
import by.my.elections.domain.model.UserRole
import by.my.elections.domain.usecases.*
import by.my.elections.presentation.base.BasePresenter
import com.tbruyelle.rxpermissions2.Permission
import io.reactivex.*
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MainPresenter(
    private val getLastKnownLocationUseCase: GetLastKnownLocationUseCase,
    private val userRoleUseCase: UserRoleUseCase,
    private val activateTrackingUseCase: ActivateTrackingUseCase,
    private val deactivateTrackingUseCase: DeactivateTrackingUseCase,
    private val schedulerProvider: SchedulerProvider
) : BasePresenter<MainPresenter.View>() {

    override fun onViewAttached(view: View) {
        super.onViewAttached(view)
        disposeOnViewDetached(
            view.onMapReady()
                .switchMap { view.requestPermission() }
                .switchMapSingle {
                    if (it.granted) {
                        getLastKnownLocationUseCase.execute()
                    } else {
                        Single.error(Throwable("Permission isn't granted"))
                    }
                }
                .subscribeOn(schedulerProvider.ui())
                .subscribe({
                    view.showDeviceLocation(it)
                    Timber.d("OnMapReady: %s", it)
                }, {
                    Timber.e(it)
                })
        )

        disposeOnViewDetached(
            view.toggle()
                .switchMapCompletable {
                    if (it) {
                        Timber.d("L: activateTrackingUseCase")
                        activateTrackingUseCase.execute()
                    } else {
                        Timber.d("L: deactivateTrackingUseCase")
                        deactivateTrackingUseCase.execute()
                    }
                }
                .observeOn(schedulerProvider.ui())
                .retry()
                .subscribe({
                    Timber.d("Toggle state")
                }, {
                    Timber.e(it)
                })
        )
    }

    override fun onViewWillShow(view: View) {
        super.onViewWillShow(view)
        disposeOnViewWillHide(
            view.onActivateClick()
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    view.switchActiveBottomSheet()
                }, {
                    Timber.e(it)
                })
        )


        disposeOnViewWillHide(
            Observable.combineLatest(view.toggle(), view.locationState(),
                BiFunction { isToggleOn: Boolean, locationState: LocationState ->
                    when (locationState) {
                        LocationState.NoPermission -> TrackModeStatus.PermissionNotGranted
                        LocationState.NoLocation -> TrackModeStatus.NoLocation
                        LocationState.HasLocation -> if (isToggleOn) TrackModeStatus.On else TrackModeStatus.Off
                    }
                })
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    view.setTrackMode(it)
                }, {
                    Timber.e(it)
                })
        )

        disposeOnViewWillHide(
            view.onSupportClick()
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    view.switchSupportBottomSheet()
                }, {
                    Timber.e(it)
                })
        )



        disposeOnViewWillHide(
            userRoleUseCase.execute()
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    when (it) {
                        UserRole.Member -> {
                            view.showActiveButton()
                        }
                        else -> {
                            view.showSupportButton()
                        }
                    }
                }, {
                    Timber.e(it)
                })
        )
    }

    private fun View.locationState(): Observable<LocationState> = hasPermission()
        .flatMapObservable {
            if (it) {
                Observable.just(true)
            } else {
                requestPermission().map { permission -> permission.granted }
            }
        }
        .switchMap {
            if (it) {
                getLastKnownLocationUseCase.execute().map { true }.toObservable().startWith(false)
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .map { hasLocation ->
                        if (hasLocation) {
                            LocationState.HasLocation
                        } else {
                            LocationState.NoLocation
                        }
                    }
            } else {
                Observable.just(LocationState.NoPermission)
            }
        }

    enum class LocationState {
        NoPermission,
        NoLocation,
        HasLocation
    }

    interface View {
        fun onMapReady(): Observable<Unit>
        fun requestPermission(): Observable<Permission>
        fun showDeviceLocation(deviceLocation: DeviceLocation)
        fun showActiveButton()
        fun switchActiveBottomSheet()
        fun showSupportButton()
        fun switchSupportBottomSheet()
        fun onSupportClick(): Observable<Unit>
        fun onActivateClick(): Observable<Unit>
        fun setTrackMode(mode: TrackModeStatus)
        fun toggle(): Observable<Boolean>
        fun hasPermission(): Single<Boolean>
    }
}